# powerdisplay.kicad_sch BOM

Fri 27 Oct 2023 09:06:50 PM EDT

Generated from schematic by Eeschema 7.0.8-7.0.8~ubuntu22.04.1

**Component Count:** 9

| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| H3, H4 | 2 | Led_Hole |  |  |  |
| J1 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | DigiKey | 1175-1621-ND |
| J2 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |
| R1, R2 | 2 | RL | Resistor | Tayda | A-3073 |
| TP1 | 1 | GND | test point |  |  |
| TP2 | 1 | +12V | test point |  |  |
| TP3 | 1 | -12V | test point |  |  |

## Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|RL|2|R1, R2|

