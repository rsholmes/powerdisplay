# Power Display

This is a module sort of in Kosmo format. "Sort of" because it is 20 mm wide, because that was the amount of space I had left at the end of each row. All it is is a panel with, on the back, a surface mount 10-position header to connect power, two surface mount resistors, and two through hole LEDs (but with the leads bent over to solder to surface pads); and on the front, three test points, and a through hole 10-position header. The LEDs indicate power on each rail. The test points allow measuring voltages on each rail. The front-mounted header permits easy power connection for a module being tested or undergoing trouble shooting.

It probably is a good idea to use a spare IDC connector to cover the front-mounted header when it is not in use. 

## Current draw
5 mA +12 V, 5 mA -12 V


## Photos

![](Images/pdfront.jpg)

![](Images/pdback.jpg)

## Documentation

* [Schematic](Docs/powerdisplay_schematic.pdf)
* PCB layout: [front](Docs/Layout/powerdisplay/powerdisplay_front.svg), [back](Docs/Layout/powerdisplay/powerdisplay_back.svg)
* [BOM](Docs/BOM/powerdisplay_bom.md)
* [Blog post](https://www.richholmes.xyz/analogoutput/2023-10-30_display-and-slope/)

## Git repository

* [https://gitlab.com/rsholmes/powerdisplay](https://gitlab.com/rsholmes/powerdisplay)

